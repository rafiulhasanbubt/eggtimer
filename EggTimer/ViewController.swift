//
//  ViewController.swift
//  EggTimer
//
//  Created by rafiul hasan on 12/20/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var softEggView: UIView!
    @IBOutlet weak var mediumEggView: UIView!
    @IBOutlet weak var hardEggView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    var eggTimes = ["Soft": 5, "Medium": 7, "Hard": 12]
    var timer = Timer()
    var player: AVAudioPlayer!
    var totalTime = 0
    var secondsPassed = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        softEggView.layer.cornerRadius = 10
        softEggView.layer.borderColor = UIColor.white.cgColor
        softEggView.layer.borderWidth = 1.0
        
        mediumEggView.layer.cornerRadius = 10
        mediumEggView.layer.borderColor = UIColor.white.cgColor
        mediumEggView.layer.borderWidth = 1.0
        
        hardEggView.layer.cornerRadius = 10
        hardEggView.layer.borderColor = UIColor.white.cgColor
        hardEggView.layer.borderWidth = 1.0

    }

    @IBAction func HardnessSelected(_ sender: UIButton) {
        timer.invalidate()
        let hardness = sender.currentTitle!
        totalTime = eggTimes[hardness]!
        
        progressBar.progress = 0.0
        secondsPassed = 0
        titleLabel.text = hardness

        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        if secondsPassed < totalTime {
            secondsPassed += 1
            progressBar.progress = Float(secondsPassed) / Float(totalTime)
            print(Float(secondsPassed) / Float(totalTime))
        } else {
            timer.invalidate()
            titleLabel.text = "DONE!"
            
            guard let url = Bundle.main.url(forResource: "alarm_sound", withExtension: "mp3") else { return }
            player = try! AVAudioPlayer(contentsOf: url)
            player.play()
        }
    }
}

